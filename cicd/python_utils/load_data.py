""" Copyright (C) 2024  Matti Kaupenjohann

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from dataclasses import dataclass, field
from typing import Any, Dict
import json
from pathlib import Path

@dataclass
class WikiDataloader:
    report_path: Path
    report: Dict[str, Any] = field(default_factory=dict)
    kroki_graph: Dict[str, Any] = field(default_factory=dict)
    table: Dict[str, Any] = field(default_factory=dict)
    md: Dict[str, Any] = field(default_factory=dict)

    def generate_content(self):
        self._load_report()
        self._load_templates()

        # Generate Shields and first Header
        status_color = "green" if self.report["scan"]["status"] == "success" else "red"

        markdown_string = (
            f'![Version]({self._generate_shield("version")})'
            f'![Version]({self._generate_shield("scanner")})'
            f'![Version]({self._generate_shield("analyzer")})\n\n'
            f'![Version]({self._generate_shield("start_time")})'
            f'![Version]({self._generate_shield("end_time")})'
            f'![Version]({self._generate_shield("status", status_color)})\n\n'
            f'{self.md["heading1"]}\n\n'
        )

        # Count Vulnerabilities and create Vulnerbility Lists
        vulnerability_counts = {
            "Critical": 0,
            "High": 0,
            "Medium": 0,
            "Low": 0,
            "Unknown": 0,
            "Info": 0
        }
        vulnerability_lists = {
            "Critical": [],
            "High": [],
            "Medium": [],
            "Low": [],
            "Unknown": [],
            "Info": []
        }
        for idx, vul in enumerate(self.report["vulnerabilities"]):
            severity = vul["severity"]
            cve = vul["cve"].rpartition(':')[-1]
            if "message" in vul:
                message = vul["message"].replace("\n", " ")
            else:
                message = " "
            if "description" in vul:
                desc = vul["description"].replace("\n", " ")
            else:
                desc = " "
            location = f'{vul["location"]["file"]}:{vul["location"]["start_line"]}'

            vulnerability_counts[vul["severity"]] += 1
            vulnerability_lists[vul["severity"]].append(
                f"| {idx} | {severity} | {cve} | {message} | {desc} | {location} |"
            )

        # Replace Counts in graph and append markdown_string
        for severity in vulnerability_counts:
            for content in self.kroki_graph["data"][0]["values"]:
                if content["category"] == severity:
                    idx = self.kroki_graph["data"][0]["values"].index(content)
                    break
            self.kroki_graph["data"][0]["values"][idx]["amount"] = vulnerability_counts[severity]

        markdown_string = (
            f"{markdown_string}"
            "```vega\n"
            f"{json.dumps(self.kroki_graph, indent=4)}\n"
            "```\n\n"
        )

        # Generate table and second heading
        markdown_string = (
            f"{markdown_string}"
            f'{self.md["heading2"]}\n\n'
            f'{self.table["header"]}\n'
            f'{self.table["split"]}\n'
        )

        for severity in vulnerability_lists:
            markdown_string = (
                f"{markdown_string}"
                f"|| {severity} |||||\n"
            )
            for line in vulnerability_lists[severity]:
                markdown_string = (
                f"{markdown_string}"
                f"{line}\n"
            )

        return markdown_string

    def _load_report(self):
        with open(self.report_path, "r") as report_file:
            self.report = json.load(report_file)

    def _load_templates(self):
        template_dir = Path(__file__).parent.parent / "templates"
        kroki_template = template_dir / "kroki_barplot_sast.json"
        table_template = template_dir / "table_sast.json"
        md_template = template_dir / "sast_report_md.json"
        with open(kroki_template, "r") as kroki_file:
            self.kroki_graph = json.load(kroki_file)
        with open(table_template, "r") as table_file:
            self.table = json.load(table_file)
        with open(md_template, "r") as md_file:
            self.md = json.load(md_file)

    def _generate_shield(self, name:str, color=None) -> str:
        shield_name = f"shield_{name}"
        shield_part1 = self.md[shield_name][0]
        shield_part2 = self.md[shield_name][1]
        if name == "version":
            shield_value = self.report[name]
        elif name in ["analyzer", "scanner"]:
            shield_value = self.report["scan"][name]["name"]
        elif color:
            shield_value = f'{self.report["scan"][name]}-{color}'
        else:
            shield_value = self.report["scan"][name].replace("-","--")

        return f"{shield_part1}{shield_value}{shield_part2}"


if __name__ == "__main__":
    report_path = Path.home() / "Downloads/gl-sast-report.json"
    loader = WikiDataloader(report_path)
    print(loader.generate_content())
