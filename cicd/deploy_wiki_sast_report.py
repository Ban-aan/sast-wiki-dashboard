""" Copyright (C) 2024  Matti Kaupenjohann

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import os, argparse
from pathlib import Path

import gitlab

from python_utils.load_data import WikiDataloader

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('report', metavar='report-file', type=str,
                    help='path to the report to parse', nargs='?', default="gl-sast-report.json")

    parser.add_argument('wiki_page', metavar='wiki-page-name', type=str,
                    help='name of the wiki page to publish to', nargs='?', default="SAST REPORT")

    args = parser.parse_args()

    gitlab_url = os.environ.get('CI_SERVER_URL') or "https://gitlab.com"
    gl = gitlab.Gitlab(gitlab_url, private_token=os.environ.get('SAST_REPORT'))
    project_id = os.environ.get('CI_PROJECT_ID')
    project = gl.projects.get(project_id)
    pages = project.wikis.list()
    sast_report_path = Path(os.environ.get('CI_PROJECT_DIR')) / args.report
    content_loader = WikiDataloader(sast_report_path)
    page_titles = dict()
    if pages:
        for idx, page in enumerate(pages):
            page_titles[page.slug] = idx

    wiki_page_slug = args.wiki_page.replace(" ", "-")

    if wiki_page_slug in page_titles.keys():
        idx = page_titles[wiki_page_slug]
        # Workaround for: https://github.com/python-gitlab/python-gitlab/issues/2706
        pages[idx].title = pages[idx].title
        pages[idx].content = content_loader.generate_content()
        pages[idx].save()
    else:
        title = args.wiki_page
        content = content_loader.generate_content()
        page = project.wikis.create(
            {
                "title": title,
                "content": content
            }
        )

if __name__ == "__main__":
    main()
